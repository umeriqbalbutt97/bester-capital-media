package com.smartherd.bestercapitalmedia.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.smartherd.bestercapitalmedia.R;
import com.smartherd.bestercapitalmedia.model.ProfileModel;

import java.util.List;

public class ManagerEmployeeAdapter extends RecyclerView.Adapter<ManagerEmployeeAdapter.ManagerEmployeeViewHolder> {

    Context context;
    List<ProfileModel> modelList;
    ClickListenerInterface clickListenerInterface;

    public ManagerEmployeeAdapter(Context context, List<ProfileModel> modelList,ClickListenerInterface clickListenerInterface) {
        this.context = context;
        this.modelList = modelList;
        this.clickListenerInterface=clickListenerInterface;
    }

    @NonNull
    @Override
    public ManagerEmployeeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_item, parent, false);
        return new ManagerEmployeeAdapter.ManagerEmployeeViewHolder(view,clickListenerInterface);

    }

    @Override
    public void onBindViewHolder(@NonNull ManagerEmployeeViewHolder holder, int position) {
        final ProfileModel profileModel = modelList.get(position);

        Glide.with(context).load(profileModel.getImg_url()).into(holder.imageView);
        holder.name.setText(profileModel.getF_name() + " " + profileModel.getL_name());

    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    public class ManagerEmployeeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imageView;
        TextView name;
        ClickListenerInterface clickListenerInterface;

        public ManagerEmployeeViewHolder(@NonNull View itemView,ClickListenerInterface clickListenerInterface) {
            super(itemView);

            name = itemView.findViewById(R.id.personname);
            imageView = itemView.findViewById(R.id.personimage);
            this.clickListenerInterface=clickListenerInterface;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListenerInterface.OnItemClickListener(getAdapterPosition());
        }
    }

    public interface ClickListenerInterface{
        void OnItemClickListener(int position);
    }
}
