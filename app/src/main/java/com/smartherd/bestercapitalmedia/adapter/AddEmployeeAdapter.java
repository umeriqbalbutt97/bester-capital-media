package com.smartherd.bestercapitalmedia.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.smartherd.bestercapitalmedia.R;
import com.smartherd.bestercapitalmedia.model.ProfileModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class AddEmployeeAdapter extends RecyclerView.Adapter<AddEmployeeAdapter.AddEmployeeViewHolder> {

    Context context;
    List<ProfileModel> profileModelList;
    boolean flag;


    public AddEmployeeAdapter(Context context, List<ProfileModel> profileModelList) {
        this.context = context;
        this.profileModelList = profileModelList;
    }

    @NonNull
    @Override
    public AddEmployeeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.add_employee_recycler_view_layout, parent, false);
        return new AddEmployeeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AddEmployeeViewHolder holder, int position) {
        final ProfileModel profileModel=profileModelList.get(position);

        Glide.with(context).load(profileModel.getImg_url()).into(holder.imageView);
        holder.name.setText(profileModel.getF_name()+" "+profileModel.getL_name());

        try {
            flag = profileModel.getManagers().get(FirebaseAuth.getInstance().getUid());
        }catch (NullPointerException e){
            e.printStackTrace();
        }
        if(flag){
            holder.checkBox.setChecked(true);
            flag=false;
        }

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked){
                    Map<String, Object> map = new HashMap<>();
                    map.put(FirebaseAuth.getInstance().getUid(), true);
                    FirebaseDatabase.getInstance().getReference().child("Profiles").child(profileModel.getUid()).child("managers").updateChildren(map);
                }else{
                    Map<String, Object> map = new HashMap<>();
                    map.put(FirebaseAuth.getInstance().getUid(), false);
                    FirebaseDatabase.getInstance().getReference().child("Profiles").child(profileModel.getUid()).child("managers").updateChildren(map);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return profileModelList.size();
    }

    public class AddEmployeeViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView name;
        CheckBox checkBox;

        public AddEmployeeViewHolder(@NonNull View itemView) {
            super(itemView);

            name=itemView.findViewById(R.id.personname);
            imageView=itemView.findViewById(R.id.personimage);
            checkBox=itemView.findViewById(R.id.personcheckbox);

        }
    }

}
