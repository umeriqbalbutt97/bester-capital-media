package com.smartherd.bestercapitalmedia.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.smartherd.bestercapitalmedia.R;
import com.smartherd.bestercapitalmedia.model.LeaveModel;
import com.smartherd.bestercapitalmedia.view.LeaveDetailActivity;

import java.util.List;

public class EmployeeLeaveHistoryAdapter extends RecyclerView.Adapter<EmployeeLeaveHistoryAdapter.EmployeeLeaveHistoryViewHolder> {
    List<LeaveModel> leaveModels;
    Context context;

    public EmployeeLeaveHistoryAdapter(List<LeaveModel> leaveModels, Context context) {
        this.leaveModels = leaveModels;
        this.context = context;
    }

    @NonNull
    @Override
    public EmployeeLeaveHistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.leave_history_item, parent, false);
        return new EmployeeLeaveHistoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EmployeeLeaveHistoryViewHolder holder, int position) {
        final LeaveModel leaveModel=leaveModels.get(position);
        holder.from.setText("from: "+leaveModel.getFrom());
        holder.to.setText("to: "+leaveModel.getTo());
        holder.type.setText("type: "+leaveModel.getType());
        holder.status.setText(leaveModel.getFinal_status());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, LeaveDetailActivity.class);
                intent.putExtra("obj",leaveModel);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return leaveModels.size();
    }

    public class EmployeeLeaveHistoryViewHolder extends RecyclerView.ViewHolder {

        TextView from,to,status,type;
        public EmployeeLeaveHistoryViewHolder(@NonNull View itemView) {
            super(itemView);
            from=itemView.findViewById(R.id.from);
            to=itemView.findViewById(R.id.to);
            type=itemView.findViewById(R.id.type);
            status=itemView.findViewById(R.id.status);
        }
    }
}
