package com.smartherd.bestercapitalmedia.adapter;

import android.content.Context;
import android.content.Intent;
import android.provider.SyncStateContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.smartherd.bestercapitalmedia.R;
import com.smartherd.bestercapitalmedia.model.LeaveModel;
import com.smartherd.bestercapitalmedia.model.LeaveQoutaModel;
import com.smartherd.bestercapitalmedia.model.ProfileModel;
import com.smartherd.bestercapitalmedia.utils.Utilities;
import com.smartherd.bestercapitalmedia.view.LeaveDetailActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ManagerEmployeeLeaveAdapter extends RecyclerView.Adapter<ManagerEmployeeLeaveAdapter.ManagerEmployeeLeaveViewHolder> {
    Context context;
    List<LeaveModel> leaveModels;
    LeaveModel leaveModel;

    public ManagerEmployeeLeaveAdapter(Context context, List<LeaveModel> leaveModels) {
        this.context = context;
        this.leaveModels = leaveModels;
    }

    @NonNull
    @Override
    public ManagerEmployeeLeaveViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.manager_employee_item, parent, false);
        return new ManagerEmployeeLeaveViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ManagerEmployeeLeaveViewHolder holder, int position) {

        leaveModel=leaveModels.get(position);
        holder.from.setText("from: "+leaveModel.getFrom());
        holder.to.setText("to: " +leaveModel.getTo());
        holder.type.setText(leaveModel.getType());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, LeaveDetailActivity.class);
                intent.putExtra("obj",leaveModel);
                context.startActivity(intent);
            }
        });

        holder.approve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, Object> map= new HashMap<>();
                map.put(FirebaseAuth.getInstance().getUid(),"approved");
                FirebaseDatabase.getInstance().getReference().child("leaves").child(leaveModel.getUid()).child(leaveModel.getPush_id()).child("status").updateChildren(map);
                fetchLeaveOfManagerEmployee();
            }

        });

        holder.reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, Object> map= new HashMap<>();
                map.put(FirebaseAuth.getInstance().getUid(),"rejected");
                FirebaseDatabase.getInstance().getReference().child("leaves").child(leaveModel.getUid()).child(leaveModel.getPush_id()).child("status").updateChildren(map);
                fetchLeaveOfManagerEmployee();
            }
        });

    }

    @Override
    public int getItemCount() {
        return leaveModels.size();
    }

    public class ManagerEmployeeLeaveViewHolder extends RecyclerView.ViewHolder {
        TextView from,to,type;
        Button reject,approve;

        public ManagerEmployeeLeaveViewHolder(@NonNull View itemView) {
            super(itemView);
            from=itemView.findViewById(R.id.from);
            to=itemView.findViewById(R.id.to);
            type=itemView.findViewById(R.id.type);
            reject=itemView.findViewById(R.id.reject);
            approve=itemView.findViewById(R.id.approve);
        }
    }


    public void rejectOrApproveLeave(LeaveModel leaveModel){
        int app=0;
        int rej=0;
        int pend=0;

        for(Map.Entry<String, String> entry :  leaveModel.getStatus().entrySet()){
            Log.d(Utilities.TAG, "rejectOrApproveLeave: "+entry.getValue()+"  "+entry.getKey());
            if(entry.getValue().equals("approved")) {
                Log.d(Utilities.TAG, "rejectOrApproveLeave: in app "+entry.getValue()+"  "+entry.getKey());
                app++;
            }else if(entry.getValue().equals("pending")){
                Log.d(Utilities.TAG, "rejectOrApproveLeave: in pen "+entry.getValue()+"  "+entry.getKey());
                pend++;
            }
            else{
                Log.d(Utilities.TAG, "rejectOrApproveLeave: in rej "+entry.getValue()+"  "+entry.getKey());
                rej++;
            }
        }

        Log.d(Utilities.TAG, "onDataChange: yoo 1");
        if(pend==0&&rej==0&&app>0) {
            Log.d(Utilities.TAG, "onDataChange: yoo 2");
            FirebaseDatabase.getInstance().getReference().child("leaves").child(leaveModel.getUid()).child(leaveModel.getPush_id()).child("final_status").setValue("approved");
            selectLeaveQuotaNode(leaveModel);
        }else if(pend>0){
            Log.d(Utilities.TAG, "onDataChange: yoo 3");
            FirebaseDatabase.getInstance().getReference().child("leaves").child(leaveModel.getUid()).child(leaveModel.getPush_id()).child("final_status").setValue("pending");
        }else{
            Log.d(Utilities.TAG, "onDataChange: yoo 4");
            FirebaseDatabase.getInstance().getReference().child("leaves").child(leaveModel.getUid()).child(leaveModel.getPush_id()).child("final_status").setValue("rejected");
        }
    }



    String type;
    public void selectLeaveQuotaNode(LeaveModel leaveModel){
        Log.d(Utilities.TAG, "onDataChange: yoo 5");
        if(leaveModel.getType().equals("Short Leave")){
            type="short";
            Log.d(Utilities.TAG, "onDataChange: yoo 6");
            fetchQuota(type);
        }else if(leaveModel.getType().equals("Casual Leave")){
            type="casual";
            Log.d(Utilities.TAG, "onDataChange: yoo 7");
            fetchQuota(type);
        }else if(leaveModel.getType().equals("Sick Leave")){
            type="sick";
            Log.d(Utilities.TAG, "onDataChange: yoo 8");
            fetchQuota(type);
        }else if(leaveModel.getType().equals("Annual Leave")){
            type="annual";
            Log.d(Utilities.TAG, "onDataChange: yoo 9");
            fetchQuota(type);
        }
    }

    public void fetchQuota(final String q_type){
        FirebaseDatabase.getInstance().getReference().child("leavesquota").child(leaveModel.getUid()).child(q_type).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                LeaveQoutaModel leaveQoutaModel=snapshot.getValue(LeaveQoutaModel.class);
                leaveQoutaModel.setRemaining_leaves(leaveQoutaModel.getRemaining_leaves()-1);
                FirebaseDatabase.getInstance().getReference().child("leavesquota").child(leaveModel.getUid()).child(q_type).setValue(leaveQoutaModel);
                Log.d(Utilities.TAG, "onDataChange: yoo 10");
                fetchProfile();
                Log.d(Utilities.TAG, "onDataChange: yoo 11");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void fetchLeaveOfManagerEmployee() {
        FirebaseDatabase.getInstance().getReference().child("leaves").child(leaveModel.getUid()).child(leaveModel.getPush_id()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                leaveModel=snapshot.getValue(LeaveModel.class);
                rejectOrApproveLeave(leaveModel);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
    public void fetchProfile(){
        FirebaseDatabase.getInstance().getReference().child("Profiles").child(leaveModel.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                ProfileModel profileModel=snapshot.getValue(ProfileModel.class);
                Log.d(Utilities.TAG, "onDataChange: 88"+leaveModel.getUid()+" "+profileModel.getToken_id());
                sendNotification(profileModel.getToken_id());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
    public void sendNotification(String token) {

        Log.d(Utilities.TAG, "in sendNotification: ");
        JSONObject jsonObject1 = new JSONObject();
        JSONObject jsonObject2 = new JSONObject();
        try {

            jsonObject2.put("body", "Notification");
            jsonObject2.put("title", "Response from Manager Arrives");

            JSONObject jsonObject3 = new JSONObject();

            jsonObject3.put("priority", "high");


            jsonObject1.put("to", token);
            jsonObject1.put("collapse_key", "type_a");
            jsonObject1.put("data", jsonObject2);
            jsonObject1.put("android", jsonObject3);
            Log.d(Utilities.TAG, "in sendNotification: ");

        } catch (JSONException e) {

        }

        RequestQueue queue = Volley.newRequestQueue(context);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "https://fcm.googleapis.com/fcm/send", jsonObject1, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(Utilities.TAG, "onResponse: ");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(Utilities.TAG, "onErrorResponse: ");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "key=AAAAp9eyAuE:APA91bHWKMi64fNkwKYoh76Z7kSWPMTJMpnlf8axswDG5eeDTyip1M2SxJpYVsdqrmaZOk06Oc63kcx4Z6yaIZOHLCZj73onkRuq44pO94W-yWZsS4SOmHlqNGrJVo2wEODtwJXcGrIG");
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        queue.add(jsonObjectRequest);
        jsonObjectRequest.setShouldCache(false);
    }
}
