package com.smartherd.bestercapitalmedia.utils;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Utilities {
    public static final String TAG = "TAG";
    public static final int CAMERA_REQUEST_CODE = 5146;
    public static final int GALLERY_REQUEST_CODE = 2162;
    public static final int PERMISSIONS_REQUEST_CODE=2069;

}
