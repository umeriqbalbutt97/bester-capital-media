package com.smartherd.bestercapitalmedia.fragmentview;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.smartherd.bestercapitalmedia.R;
import com.smartherd.bestercapitalmedia.adapter.ManagerEmployeeLeaveAdapter;
import com.smartherd.bestercapitalmedia.model.LeaveModel;
import com.smartherd.bestercapitalmedia.utils.Utilities;

import java.util.ArrayList;
import java.util.List;


public class ManagerLeavesFragment extends Fragment {

    LeaveModel leaveModel;
    List<LeaveModel> models = new ArrayList<>();
    RecyclerView recyclerView;
    ManagerEmployeeLeaveAdapter managerEmployeeLeaveAdapter;

    public ManagerLeavesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fetchLeaveOfManagerEmployee();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_leaves, container, false);
        recyclerView=view.findViewById(R.id.recyclerview);
        return view;
    }

    public void fetchLeaveOfManagerEmployee() {
        FirebaseDatabase.getInstance().getReference().child("leaves").orderByChild("status").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                models.clear();
                boolean flag=false;
                String uid=FirebaseAuth.getInstance().getUid();
                for (DataSnapshot child : snapshot.getChildren()) {
                    for (DataSnapshot sub:child.getChildren()){
                            leaveModel = sub.getValue(LeaveModel.class);
                            try {
                                flag = leaveModel.getStatus().get(uid).equals("pending");
                            }catch (NullPointerException e){
                                e.printStackTrace();
                            }
                             if (flag) {
                                 Log.d(Utilities.TAG, "onDataChange: lol");
                                 models.add(leaveModel);
                                 flag=false;
                            }
                    }
                }
                bakeRecyclerView();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void bakeRecyclerView(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        managerEmployeeLeaveAdapter=new ManagerEmployeeLeaveAdapter(getContext(),models);
        recyclerView.setAdapter(managerEmployeeLeaveAdapter);
    }
}