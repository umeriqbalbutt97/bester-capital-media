package com.smartherd.bestercapitalmedia.fragmentview;

import android.app.DatePickerDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.smartherd.bestercapitalmedia.R;
import com.smartherd.bestercapitalmedia.model.LeaveModel;
import com.smartherd.bestercapitalmedia.model.LeaveQoutaModel;
import com.smartherd.bestercapitalmedia.model.ProfileModel;
import com.smartherd.bestercapitalmedia.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class EmployeeLeaveFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    int maxprogress = 0;
    EditText from, to, hours, reasonedittext;
    int progressFlag=0;
    Button submit;
    final Calendar myCalendar = Calendar.getInstance();
    int check = 0;
    Spinner spinner;
    String type;
    LeaveModel leaveModel;
    List<String> empManagers = new ArrayList<>();
    List<ProfileModel> profileModelList = new ArrayList<>();
    ProfileModel mangerProfileModel;
    List<String> allTokens = new ArrayList<>();
    ProfileModel profileModel;
    private static final String[] paths = {"Short Leave", "Casual Leave", "Sick Leave", "Annual Leaves"};
    TextView totalleaves,tshort,tcasual,tannual,tsick;
    LinearLayout.LayoutParams params ;
    ProgressBar psick,pcausal,pannual,pshort;
            DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            if (year < Calendar.getInstance().get(Calendar.YEAR) || monthOfYear < Calendar.getInstance().get(Calendar.MONTH)) {
                Toast.makeText(getContext(), "Please Select the valid date", Toast.LENGTH_SHORT).show();
            } else {
                if (dayOfMonth < Calendar.getInstance().get(Calendar.DAY_OF_MONTH) && monthOfYear <= Calendar.getInstance().get(Calendar.MONTH)) {
                    Toast.makeText(getContext(), "Please Select the valid date", Toast.LENGTH_SHORT).show();
                } else {
                    updateLabel();
                }
            }
        }

    };


    public EmployeeLeaveFragment() {
        // Required empty public constructor
    }
String uid;
    @Override
    public void onStart() {
        super.onStart();
        maxprogress=0;
        uid=FirebaseAuth.getInstance().getUid();
        getShortQuota();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        getMyProfile();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_employee_leave, container, false);
        from = view.findViewById(R.id.from);
        to = view.findViewById(R.id.to);
        hours = view.findViewById(R.id.hours);
        submit = view.findViewById(R.id.submit);
        spinner = view.findViewById(R.id.type);
        reasonedittext = view.findViewById(R.id.reason);
        totalleaves=view.findViewById(R.id.totalleaves);
        tshort=view.findViewById(R.id.txtshort);
        tcasual=view.findViewById(R.id.txtscasual);
        tsick=view.findViewById(R.id.txtsick);
        tannual=view.findViewById(R.id.txtannual);
        pshort=view.findViewById(R.id.pshort);
        psick=view.findViewById(R.id.psick);
        pcausal=view.findViewById(R.id.pcasual);
        pannual=view.findViewById(R.id.pannual);

        spinnerFun();



        from.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(getContext(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                check = 1;
            }
        });

        to.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(getContext(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                check = 2;
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                empManagers.clear();
                allTokens.clear();
                profileModelList.clear();
                String push = FirebaseDatabase.getInstance().getReference().push().getKey();
                String reason = reasonedittext.getText().toString();
                String vto = to.getText().toString();
                String vhours = hours.getText().toString();
                String vfrom = from.getText().toString();
                String uid = FirebaseAuth.getInstance().getUid();

                if (reason.isEmpty() || type.isEmpty() || vfrom.isEmpty() || vto.isEmpty() || (spinner.getSelectedItemPosition() == 0 && vhours.isEmpty())) {
                    Toast.makeText(getContext(), "Please fill all fields", Toast.LENGTH_SHORT).show();
                } else {
                    getAllManagers();
                    Map<String, String> map = new HashMap<>();
                    int i = profileModel.getManagers().size();
                    Log.d(Utilities.TAG, "onClick: " + i);
                    for (Map.Entry<String, Boolean> entry : profileModel.getManagers().entrySet()) {
                        if (entry.getValue()) {
                            map.put(entry.getKey(), "pending");
                            empManagers.add(entry.getKey());
                        }
                    }
                    leaveModel = new LeaveModel(uid, push, reason, "pending", map, type, vfrom, vto, vhours);
                    FirebaseDatabase.getInstance().getReference().child("leaves").child(uid).child(push).setValue(leaveModel);
                    reasonedittext.setText("");
                    to.setText("");
                    from.setText("");
                    hours.setText("");
                }
            }
        });

        return view;
    }

    public void spinnerFun() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, paths);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
    }

    private void updateLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        if (check == 1) {
            from.setText(sdf.format(myCalendar.getTime()));
        } else if (check == 2) {
            to.setText(sdf.format(myCalendar.getTime()));
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        type = paths[position];
        if (position == 0) {
            hours.setVisibility(View.VISIBLE);
        } else {
            hours.setVisibility(View.GONE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        type = "";
    }

    public void getMyProfile() {

        FirebaseDatabase.getInstance().getReference().child("Profiles").child(FirebaseAuth.getInstance().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                profileModel = snapshot.getValue(ProfileModel.class);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void sendLeaveNotifications() {
        Log.d(Utilities.TAG, "sendLeaveNotifications: ");
        for (int itr = 0; itr < allTokens.size(); itr++) {
            sendNotification(allTokens.get(itr));
            Log.d(Utilities.TAG, "sendLeaveNotifications: ");

        }
    }
    public void sendNotification(String token) {
        Log.d(Utilities.TAG, "sendNotification: ");
        JSONObject jsonObject1 = new JSONObject();
        JSONObject jsonObject2 = new JSONObject();
        try {

            jsonObject2.put("body", "Notification");
            jsonObject2.put("title", "New Leaves Arrives");

            JSONObject jsonObject3 = new JSONObject();

            jsonObject3.put("priority", "high");


            jsonObject1.put("to", token);
            jsonObject1.put("collapse_key", "type_a");
            jsonObject1.put("data", jsonObject2);
            jsonObject1.put("android", jsonObject3);

        } catch (JSONException e) {

        }

        RequestQueue queue = Volley.newRequestQueue(getContext());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "https://fcm.googleapis.com/fcm/send", jsonObject1, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(Utilities.TAG, "sendNotification: response");
                Toast.makeText(getContext(), "Your leave has been submitted", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "key=AAAAp9eyAuE:APA91bHWKMi64fNkwKYoh76Z7kSWPMTJMpnlf8axswDG5eeDTyip1M2SxJpYVsdqrmaZOk06Oc63kcx4Z6yaIZOHLCZj73onkRuq44pO94W-yWZsS4SOmHlqNGrJVo2wEODtwJXcGrIG");
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        queue.add(jsonObjectRequest);
        jsonObjectRequest.setShouldCache(false);
    }

    public void getToken() {
        for (int itr = 0; itr < empManagers.size(); itr++) {
            for (int btr = 0; btr < profileModelList.size(); btr++) {
                if (empManagers.get(itr).equals(profileModelList.get(btr).getUid())) {
                    allTokens.add(profileModelList.get(btr).getToken_id());
                    Log.d(Utilities.TAG, "getToken: " + profileModelList.get(btr).getToken_id());
                }
            }
        }
        sendLeaveNotifications();
    }

    public void getAllManagers() {
        FirebaseDatabase.getInstance().getReference().child("Profiles").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot child : snapshot.getChildren()) {
                    mangerProfileModel = child.getValue(ProfileModel.class);
                    if (mangerProfileModel.getRole() == 2) {
                        profileModelList.add(mangerProfileModel);
                    }
                }
                getToken();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }



    public void getSickQuota() {
        FirebaseDatabase.getInstance().getReference().child("leavesquota").child(uid).child("sick").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                LeaveQoutaModel leaveQoutaModel = snapshot.getValue(LeaveQoutaModel.class);
                int i = 0;
                try {
                    i = leaveQoutaModel.getTotal_leaves();
                    Log.d(Utilities.TAG, "onDataChange: "+i);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                maxprogress = maxprogress + i;
                psick.setMax(leaveQoutaModel.getTotal_leaves());
                psick.setProgress(leaveQoutaModel.getTotal_leaves()-leaveQoutaModel.getRemaining_leaves());
                params = (LinearLayout.LayoutParams) psick.getLayoutParams();
                convertNumberIntoDecimal(leaveQoutaModel);
                psick.setLayoutParams(params);
                progressFlag++;
                progressBarColoring("sick",leaveQoutaModel.getRemaining_leaves());
                getCasualQuota();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void getCasualQuota() {
        FirebaseDatabase.getInstance().getReference().child("leavesquota").child(uid).child("casual").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                LeaveQoutaModel leaveQoutaModel = snapshot.getValue(LeaveQoutaModel.class);
                int i = 0;
                try {
                    i = leaveQoutaModel.getTotal_leaves();
                    Log.d(Utilities.TAG, "onDataChange: "+i);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                maxprogress = maxprogress + i;
                pcausal.setMax(leaveQoutaModel.getTotal_leaves());
                pcausal.setProgress(leaveQoutaModel.getTotal_leaves()-leaveQoutaModel.getRemaining_leaves());
                params = (LinearLayout.LayoutParams) pcausal.getLayoutParams();
                convertNumberIntoDecimal(leaveQoutaModel);
                pcausal.setLayoutParams(params);
                progressFlag++;
                progressBarColoring("casual",leaveQoutaModel.getRemaining_leaves());
                getAnnualQuota();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void getShortQuota() {
        FirebaseDatabase.getInstance().getReference().child("leavesquota").child(uid).child("short").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                LeaveQoutaModel leaveQoutaModel = snapshot.getValue(LeaveQoutaModel.class);
                int i = 0;
                try {
                    i = leaveQoutaModel.getTotal_leaves();
                    Log.d(Utilities.TAG, "onDataChange: "+i);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                maxprogress = maxprogress + i;
                progressFlag++;
                pshort.setMax(leaveQoutaModel.getTotal_leaves());
                pshort.setProgress(leaveQoutaModel.getTotal_leaves()-leaveQoutaModel.getRemaining_leaves());
                params = (LinearLayout.LayoutParams) pshort.getLayoutParams();
                convertNumberIntoDecimal(leaveQoutaModel);
                pshort.setLayoutParams(params);
                progressBarColoring("short",leaveQoutaModel.getRemaining_leaves());
                getSickQuota();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void getAnnualQuota() {
        FirebaseDatabase.getInstance().getReference().child("leavesquota").child(uid).child("annual").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                LeaveQoutaModel leaveQoutaModel = snapshot.getValue(LeaveQoutaModel.class);
                int i = 0;
                try {
                    i = leaveQoutaModel.getTotal_leaves();
                    Log.d(Utilities.TAG, "onDataChange: "+i);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }


                maxprogress = maxprogress + i;
                pannual.setMax(leaveQoutaModel.getTotal_leaves());
                pannual.setProgress(leaveQoutaModel.getTotal_leaves()-leaveQoutaModel.getRemaining_leaves());
                params = (LinearLayout.LayoutParams) pannual.getLayoutParams();
                convertNumberIntoDecimal(leaveQoutaModel);
                pannual.setLayoutParams(params);
                progressFlag++;
                progressBarColoring("annual",leaveQoutaModel.getRemaining_leaves());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
    int fshort, fsick, fcasual, fannual;


    public void convertNumberIntoDecimal(LeaveQoutaModel leaveQoutaModel){

        Log.d(Utilities.TAG, "onDataChange:w "+(leaveQoutaModel.getTotal_leaves()-leaveQoutaModel.getRemaining_leaves()));
        Log.d(Utilities.TAG, "onDataChange:w "+((float)leaveQoutaModel.getTotal_leaves()/leaveQoutaModel.getMax()));
        if(leaveQoutaModel.getTotal_leaves()>9) {
            params.weight = ((float)leaveQoutaModel.getTotal_leaves()/leaveQoutaModel.getMax());
        }else{
            params.weight = ((float)leaveQoutaModel.getTotal_leaves()/leaveQoutaModel.getMax());
        }
    }
    public void progressBarColoring(String type,int till) {

        totalleaves.setText("Total Leaves : "+maxprogress);

        if(type.equals("short")) {
            tshort.setText(String.valueOf(till));
        }else if(type.equals("sick")) {
            tsick.setText(String.valueOf(till));
        }else if(type.equals("casual")) {
            tcasual.setText(String.valueOf(till));
        }else if(type.equals("annual")) {
            tannual.setText(String.valueOf(till));
        }

    }

}