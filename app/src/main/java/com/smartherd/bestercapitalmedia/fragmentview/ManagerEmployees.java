package com.smartherd.bestercapitalmedia.fragmentview;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.smartherd.bestercapitalmedia.R;
import com.smartherd.bestercapitalmedia.adapter.ManagerEmployeeAdapter;
import com.smartherd.bestercapitalmedia.model.ProfileModel;
import com.smartherd.bestercapitalmedia.utils.Utilities;
import com.smartherd.bestercapitalmedia.view.LeavesHistoryActivity;

import java.util.ArrayList;
import java.util.List;

public class ManagerEmployees extends Fragment implements ManagerEmployeeAdapter.ClickListenerInterface {

    ProfileModel profileModel;
    List<ProfileModel> models=new ArrayList<>();
    boolean flag;
    RecyclerView recyclerView;
    ManagerEmployeeAdapter managerEmployeeAdapter;
    public ManagerEmployees() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_my_employees, container, false);
        recyclerView=view.findViewById(R.id.my_emp);
        return view;
    }
    public void bakeRecyclerView(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        managerEmployeeAdapter=new ManagerEmployeeAdapter(getContext(),models,this);
        recyclerView.setAdapter(managerEmployeeAdapter);
    }

    public void fetchManagerEmployees(){
        models.clear();
        FirebaseDatabase.getInstance().getReference().child("Profiles").orderByChild("managers").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot child: snapshot.getChildren()){
                    profileModel=child.getValue(ProfileModel.class);

                    try {
                        flag  = profileModel.getManagers().get(FirebaseAuth.getInstance().getUid());
                    }catch (NullPointerException e){
                        e.printStackTrace();
                    }

                    if(flag){
                        Log.d(Utilities.TAG, "onDataChange: "+profileModel.getF_name());
                        models.add(profileModel);
                        flag=false;
                    }
                }
                bakeRecyclerView();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchManagerEmployees();
    }

    @Override
    public void OnItemClickListener(int position) {
        Intent intent=new Intent(getContext(), LeavesHistoryActivity.class);
        intent.putExtra("obj",models.get(position));
        getActivity().startActivity(intent);
    }
}