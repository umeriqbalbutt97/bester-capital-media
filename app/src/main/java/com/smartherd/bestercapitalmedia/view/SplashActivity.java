package com.smartherd.bestercapitalmedia.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.smartherd.bestercapitalmedia.R;
import com.smartherd.bestercapitalmedia.model.ProfileModel;

import static com.smartherd.bestercapitalmedia.utils.Utilities.PERMISSIONS_REQUEST_CODE;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onStart() {
        super.onStart();
        generatedToken();
        getProfile();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("2222","Leave", NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("Leave Channel");
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }


        mPrefs = getSharedPreferences("Check", Context.MODE_PRIVATE);
        check = mPrefs.getBoolean("CHK", false);

        checkPermissions();
    }

    private void splashWorkerThread(){
        // we use thread to perform multiple operations together...

        Thread td = new Thread() {
            // run() method is used inside the Thread class
            public void run() {
                //Exception Handling
                try {
                    sleep(2000); // take param in milli seconds (3000ms = 3 seconds)
                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                   screenSelectionLogic();
                }
            }
        };
        td.start();
    }

    private void screenSelectionLogic(){
        if (check) {
            if (String.valueOf(mPrefs.getInt("GET", 0)).equals(String.valueOf(0))) {
                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            } else if (String.valueOf(mPrefs.getInt("GET", 0)).equals(String.valueOf(1))) {
                Intent intent = new Intent(SplashActivity.this, EmployeeHome.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            } else if (String.valueOf(mPrefs.getInt("GET", 0)).equals(String.valueOf(2))) {
                Intent intent = new Intent(SplashActivity.this, ManagerHome.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        } else {
            Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    }

    private void checkPermissions(){
        if(ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.CALL_PHONE)
                +ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                +ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                +ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.INTERNET)
                +ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.CAMERA)
                +ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                +ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(SplashActivity.this,Manifest.permission.CALL_PHONE)
                    ||ActivityCompat.shouldShowRequestPermissionRationale(SplashActivity.this,Manifest.permission.READ_EXTERNAL_STORAGE)
                    ||ActivityCompat.shouldShowRequestPermissionRationale(SplashActivity.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    ||ActivityCompat.shouldShowRequestPermissionRationale(SplashActivity.this,Manifest.permission.CAMERA)
                    ||ActivityCompat.shouldShowRequestPermissionRationale(SplashActivity.this,Manifest.permission.INTERNET)
                    ||ActivityCompat.shouldShowRequestPermissionRationale(SplashActivity.this,Manifest.permission.ACCESS_FINE_LOCATION)
                    ||ActivityCompat.shouldShowRequestPermissionRationale(SplashActivity.this,Manifest.permission.ACCESS_COARSE_LOCATION)){
                AlertDialog.Builder builder=new AlertDialog.Builder(SplashActivity.this);
                builder.setTitle("Grant those Permission");
                builder.setMessage("Read & Write Storage,Camera,Internet,Call,Location");
                builder.setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(SplashActivity.this,
                                new String[] {
                                        Manifest.permission.CALL_PHONE,
                                        Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                        Manifest.permission.INTERNET,
                                        Manifest.permission.CAMERA,
                                        Manifest.permission.ACCESS_FINE_LOCATION,
                                        Manifest.permission.ACCESS_COARSE_LOCATION
                                },
                                PERMISSIONS_REQUEST_CODE);
                    }
                });
                builder.setNegativeButton("Cancel",null);
                AlertDialog alertDialog=builder.create();
                alertDialog.show();
            }
            else{
                ActivityCompat.requestPermissions(SplashActivity.this,
                        new String[] {
                                Manifest.permission.CALL_PHONE,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.INTERNET,
                                Manifest.permission.CAMERA,
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION
                        },
                        PERMISSIONS_REQUEST_CODE);
            }
        }
        else{
            //Toast.makeText(getApplicationContext(),"Permission Already Granted",Toast.LENGTH_LONG).show();
            splashWorkerThread();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode==PERMISSIONS_REQUEST_CODE){
            if(grantResults.length>0
                    &&(grantResults[0]
                    +grantResults[1]
                    +grantResults[2]
                    +grantResults[3]
                    +grantResults[4]
                    +grantResults[5]
                    +grantResults[6]
                    ==PackageManager.PERMISSION_GRANTED)){
                splashWorkerThread();
            }
            else{
                splashWorkerThread();
            }
        }
    }

    ProfileModel profileModel;
    public void getProfile(){
        if(FirebaseAuth.getInstance().getUid() != null) {
            FirebaseDatabase.getInstance().getReference().child("Profiles").child(FirebaseAuth.getInstance().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    profileModel = snapshot.getValue(ProfileModel.class);
                    if(!token.equals(profileModel.getToken_id())){
                        logout();
                        Toast.makeText(SplashActivity.this, "You have sign in another device", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
    }
}