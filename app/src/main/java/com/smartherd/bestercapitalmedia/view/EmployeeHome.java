package com.smartherd.bestercapitalmedia.view;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.smartherd.bestercapitalmedia.R;
import com.smartherd.bestercapitalmedia.fragmentview.EmployeeAppraisalFragment;
import com.smartherd.bestercapitalmedia.fragmentview.EmployeeLeaveFragment;
import com.smartherd.bestercapitalmedia.model.LeaveModel;

import java.util.ArrayList;
import java.util.List;

public class EmployeeHome extends BaseActivity {

    BottomNavigationView bottomNavigationView;
    EmployeeAppraisalFragment employeeAppraisalFragment;
    EmployeeLeaveFragment employeeLeaveFragment;
    FragmentManager fm = getSupportFragmentManager();
    Fragment active;
    ImageView imageView;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_home);

        mPrefs = getSharedPreferences("Check", Context.MODE_PRIVATE);
        check = mPrefs.getBoolean("CHK", false);
        imageView=findViewById(R.id.leaveshistory);
        bottomNavigationView=findViewById(R.id.bottomnavigation);
        toolbar=findViewById(R.id.activity_toolbar);
        employeeAppraisalFragment=new EmployeeAppraisalFragment();
        employeeLeaveFragment=new EmployeeLeaveFragment();
        // compatibility

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(10f);
        }


        // Overflow menu
        fm.beginTransaction().add(R.id.frame_Employee, employeeAppraisalFragment).hide(employeeAppraisalFragment).commit();
        fm.beginTransaction().add(R.id.frame_Employee, employeeLeaveFragment).commit();
        active= employeeLeaveFragment;

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.leave_nav_icon:
                        opnFragment(employeeLeaveFragment);
                        return true;

                    case R.id.appraisal_nav_icon:
                        opnFragment(employeeAppraisalFragment);
                        return true;
                }
                return false;
            }
        });

        toolbar.inflateMenu(R.menu.overflow_menu);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                showToast(EmployeeHome.this, "You select " + item.getTitle());

                if (item.getItemId() == R.id.logout) {
                    logout();
                    // TODO Logout Code
                    return true;
                }

                return true;
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(EmployeeHome.this,EmployeeLeavesHistory.class);
                startActivity(intent);
            }
        });
    }

    // Method for opening fragments
    private void opnFragment(Fragment fragment) {
        fm.beginTransaction().hide(active).show(fragment).commit();
        active = fragment;
    }


}