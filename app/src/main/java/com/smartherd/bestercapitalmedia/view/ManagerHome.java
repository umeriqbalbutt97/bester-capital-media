package com.smartherd.bestercapitalmedia.view;


import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.smartherd.bestercapitalmedia.R;
import com.smartherd.bestercapitalmedia.fragmentview.ManagerAppraisalFragment;
import com.smartherd.bestercapitalmedia.fragmentview.ManagerLeavesFragment;
import com.smartherd.bestercapitalmedia.fragmentview.ManagerEmployees;

public class ManagerHome extends BaseActivity {


    BottomNavigationView bottomNavigationView;
    ManagerAppraisalFragment managerAppraisalFragment;
    ManagerLeavesFragment managerLeavesFragment;
    ManagerEmployees managerEmployees;
    FragmentManager fm = getSupportFragmentManager();
    Fragment active;
    FloatingActionButton floatingActionButton;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_home);
        toolbar= findViewById(R.id.activity_toolbar);
        floatingActionButton=findViewById(R.id.fab);
        mPrefs = getSharedPreferences("Check", Context.MODE_PRIVATE);
        check = mPrefs.getBoolean("CHK", false);
        bottomNavigationView=findViewById(R.id.bottomnavigation);
        managerAppraisalFragment =new ManagerAppraisalFragment();
        managerLeavesFragment =new ManagerLeavesFragment();
        managerEmployees =new ManagerEmployees();

        fm.beginTransaction().add(R.id.frame_Manager, managerAppraisalFragment).hide(managerAppraisalFragment).commit();
        fm.beginTransaction().add(R.id.frame_Manager, managerLeavesFragment).hide(managerLeavesFragment).commit();
        fm.beginTransaction().add(R.id.frame_Manager, managerEmployees).commit();
        active= managerEmployees;

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.myemp_nav_icon:
                        opnFragment(managerEmployees);
                        return true;
                    case R.id.leave_nav_icon:
                        opnFragment(managerLeavesFragment);
                        return true;

                    case R.id.appraisal_nav_icon:
                        opnFragment(managerAppraisalFragment);
                        return true;
                }
                return false;
            }
        });



        // compatibility

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(10f);
        }


        // Overflow menu

        toolbar.inflateMenu(R.menu.overflow_menu);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                showToast(ManagerHome.this, "You select " + item.getTitle());

                if (item.getItemId() == R.id.logout) {
                    logout();
                    // TODO Logout Code
                    return true;
                }

                return true;
            }
        });

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ManagerHome.this,AddEmployee.class);
                startActivity(intent);
            }
        });

    }

    private void opnFragment(Fragment fragment) {
        fm.beginTransaction().hide(active).show(fragment).commit();
        active = fragment;
    }
}