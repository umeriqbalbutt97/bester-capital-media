package com.smartherd.bestercapitalmedia.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.smartherd.bestercapitalmedia.R;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignUpActivity extends BaseActivity {

    private EditText email, password;
    private Button btnSignUp;
    private FirebaseAuth auth;
    private DatabaseReference reference;
    private ProgressBar progressBar;
    private String regex="^(.+)@(.+)$";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        // views init

        final Pattern pattern = Pattern.compile(regex);

        initViews();
        auth = FirebaseAuth.getInstance();

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String txt_email = email.getText().toString();
                String txt_password = password.getText().toString();
                Matcher matcher=pattern.matcher(txt_email);

                if (TextUtils.isEmpty(txt_email) || TextUtils.isEmpty(txt_password)) {
                    showToast(SignUpActivity.this, "All fields are required");
                } else if (txt_password.length() < 6) {
                    showToast(SignUpActivity.this, "Password must be atleast 6 characters long");
                }else if(!matcher.matches()){
                    showToast(SignUpActivity.this, "Please Write correct email address");
                }
                else {
                    register(txt_email, txt_password);
                    progressBar.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    public void register(String email, String password) {

        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser firebaseUser = auth.getCurrentUser();
                            assert firebaseUser != null;
                            String userID = firebaseUser.getUid();
                            reference = FirebaseDatabase.getInstance().getReference("Users").child(userID);

                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("id", userID);

                            reference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        progressBar.setVisibility(View.GONE);
                                        sendVerificationEmail();
                                    }
                                }
                            });

                        } else {
                            progressBar.setVisibility(View.GONE);
                            if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                                showToast(SignUpActivity.this, "You cannot use this Email");
                            }
                        }
                    }
                });

    }

    private void sendVerificationEmail() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        user.sendEmailVerification()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            // email sent
//                          after email is sent just logout the user and finish this activity
//                          FirebaseAuth.getInstance().signOut();
//                          startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
//                          finish();
                            Intent intent = new Intent(SignUpActivity.this, BasicProfileActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        } else {
                            // email not sent, so display message and restart the activity or do whatever you wish to do

                            //restart this activity
                            overridePendingTransition(0, 0);
                            finish();
                            overridePendingTransition(0, 0);
                            startActivity(getIntent());

                        }
                    }
                });
    }

    // Method for views initialization
    public void initViews() {
        this.email = findViewById(R.id.edit_email);
        this.password = findViewById(R.id.edit_password);
        this.btnSignUp = findViewById(R.id.btn_signup);
        this.progressBar = findViewById(R.id.progressBar1);
    }

    public void signIn(View view) {
        Intent intent=new Intent(SignUpActivity.this,LoginActivity.class);
        startActivity(intent);
        finish();
    }
}