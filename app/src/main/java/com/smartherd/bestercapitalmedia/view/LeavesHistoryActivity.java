package com.smartherd.bestercapitalmedia.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.smartherd.bestercapitalmedia.R;
import com.smartherd.bestercapitalmedia.adapter.LeaveHistoryAdapter;
import com.smartherd.bestercapitalmedia.adapter.ManagerEmployeeLeaveAdapter;
import com.smartherd.bestercapitalmedia.model.LeaveModel;
import com.smartherd.bestercapitalmedia.model.LeaveQoutaModel;
import com.smartherd.bestercapitalmedia.model.ProfileModel;

import java.util.ArrayList;
import java.util.List;

public class LeavesHistoryActivity extends AppCompatActivity {

    ProfileModel profileModel;
    LeaveHistoryAdapter leaveHistoryAdapter;
    RecyclerView recyclerView;
    List<LeaveModel> leaveModels = new ArrayList<>();
    ImageView profileImage;

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaves_history);
        recyclerView = findViewById(R.id.leavehistory);
        profileImage = findViewById(R.id.personimage);
        profileModel = getIntent().getParcelableExtra("obj");
        Glide.with(getApplicationContext()).load(profileModel.getImg_url()).into(profileImage);
        getEmployeeLeaveHistory(profileModel);
    }

    public void getEmployeeLeaveHistory(ProfileModel profileModel) {
        FirebaseDatabase.getInstance().getReference().child("leaves").child(profileModel.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
//                boolean flag=false;
                for (DataSnapshot child : snapshot.getChildren()) {
                    LeaveModel leaveModel = child.getValue(LeaveModel.class);
//                    try {
//                        flag = leaveModel.getStatus().containsKey(FirebaseAuth.getInstance().getUid());
//                    } catch (NullPointerException e) {
//                        e.printStackTrace();
//                    }
//                    if (flag) {
                        leaveModels.add(leaveModel);
//                    }
                }
                bakeRecyclerView();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(LeavesHistoryActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void bakeRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(LeavesHistoryActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        leaveHistoryAdapter = new LeaveHistoryAdapter(leaveModels, this);
        if (leaveModels.size() == 0) {
            Toast.makeText(this, "No Leaves", Toast.LENGTH_SHORT).show();
        }
        recyclerView.setAdapter(leaveHistoryAdapter);
    }

    public void changeQuota(View view) {

        Intent intent=new Intent(LeavesHistoryActivity.this,ChangeQuotaActivity.class);
        intent.putExtra("person",profileModel);
        startActivity(intent);
    }
}