package com.smartherd.bestercapitalmedia.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.smartherd.bestercapitalmedia.R;
import com.smartherd.bestercapitalmedia.model.LeaveModel;

public class LeaveDetailActivity extends AppCompatActivity {

    LeaveModel leaveModel;
    TextView from,to,status,type,reason;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave_detail);

        from=findViewById(R.id.from);
        to=findViewById(R.id.to);
        reason=findViewById(R.id.reason);
        type=findViewById(R.id.type);
        status=findViewById(R.id.status);

        leaveModel=getIntent().getParcelableExtra("obj");

        from.setText("From: "+leaveModel.getFrom());
        to.setText("To: "+leaveModel.getTo());
        type.setText("Type: "+leaveModel.getType());
        status.setText("Status: "+leaveModel.getFinal_status());
        reason.setText("Reason: "+leaveModel.getReason());

    }
}