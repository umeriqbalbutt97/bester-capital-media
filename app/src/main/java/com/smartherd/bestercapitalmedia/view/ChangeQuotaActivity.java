package com.smartherd.bestercapitalmedia.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.smartherd.bestercapitalmedia.R;
import com.smartherd.bestercapitalmedia.model.LeaveQoutaModel;
import com.smartherd.bestercapitalmedia.model.ProfileModel;
import com.smartherd.bestercapitalmedia.utils.Utilities;

public class ChangeQuotaActivity extends AppCompatActivity {


    EditText editTextSick,editTextShort,editTextCasual,editTextAnnual;
    ProfileModel profileModel;

    @Override
    protected void onStart() {
        super.onStart();
        getAnnualQuota();
        getCasualQuota();
        getShortQuota();
        getSickQuota();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_quota);
        editTextAnnual=findViewById(R.id.annual);
        editTextSick=findViewById(R.id.sick);
        editTextCasual=findViewById(R.id.casual);
        editTextShort=findViewById(R.id.ishort);
        profileModel=getIntent().getParcelableExtra("person");

    }


    public void getSickQuota(){
        FirebaseDatabase.getInstance().getReference().child("leavesquota").child(profileModel.getUid()).child("sick").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                LeaveQoutaModel leaveQoutaModel=snapshot.getValue(LeaveQoutaModel.class);
                int i = 0;
                try {
                    i=leaveQoutaModel.getTotal_leaves();
                }catch (NullPointerException e){
                    e.printStackTrace();
                }
                editTextSick.setText(String.valueOf(i));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
    public void getCasualQuota(){
        FirebaseDatabase.getInstance().getReference().child("leavesquota").child(profileModel.getUid()).child("casual").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                LeaveQoutaModel leaveQoutaModel=snapshot.getValue(LeaveQoutaModel.class);
                int i = 0;
                try {
                    i=leaveQoutaModel.getTotal_leaves();
                }catch (NullPointerException e){
                    e.printStackTrace();
                }
                editTextCasual.setText(String.valueOf(i));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
    public void getShortQuota(){
        FirebaseDatabase.getInstance().getReference().child("leavesquota").child(profileModel.getUid()).child("short").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                LeaveQoutaModel leaveQoutaModel=snapshot.getValue(LeaveQoutaModel.class);
                int i = 0;
                try {
                    i=leaveQoutaModel.getTotal_leaves();
                }catch (NullPointerException e){
                    e.printStackTrace();
                }
                editTextShort.setText(String.valueOf(i));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
    public void getAnnualQuota(){
        FirebaseDatabase.getInstance().getReference().child("leavesquota").child(profileModel.getUid()).child("annual").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                LeaveQoutaModel leaveQoutaModel=snapshot.getValue(LeaveQoutaModel.class);
                int i = 0;
                try {
                    i=leaveQoutaModel.getTotal_leaves();
                }catch (NullPointerException e){
                    e.printStackTrace();
                }
                editTextAnnual.setText(String.valueOf(i));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void change(View view) {
        int ishort,sick,annual,casual;

        ishort= Integer.parseInt(editTextShort.getText().toString());
        sick= Integer.parseInt(editTextSick.getText().toString());
        annual= Integer.parseInt(editTextAnnual.getText().toString());
        casual= Integer.parseInt(editTextCasual.getText().toString());

        int max=ishort+sick+annual+casual;

        Log.d(Utilities.TAG, "change: "+ishort+" "+sick+" "+annual+" "+casual);
        if(ishort>0||sick>0||annual>0||casual>0){
            LeaveQoutaModel leaveQoutaModel=new LeaveQoutaModel();
            leaveQoutaModel.setTotal_leaves(Integer.parseInt(String.valueOf(sick)));
            leaveQoutaModel.setMax(max);
            FirebaseDatabase.getInstance().getReference().child("leavesquota").child(profileModel.getUid()).child("sick").setValue(leaveQoutaModel);
            leaveQoutaModel.setTotal_leaves(Integer.parseInt(String.valueOf(ishort)));
            FirebaseDatabase.getInstance().getReference().child("leavesquota").child(profileModel.getUid()).child("short").setValue(leaveQoutaModel);
            leaveQoutaModel.setTotal_leaves(Integer.parseInt(String.valueOf(casual)));
            FirebaseDatabase.getInstance().getReference().child("leavesquota").child(profileModel.getUid()).child("casual").setValue(leaveQoutaModel);
            leaveQoutaModel.setTotal_leaves(Integer.parseInt(String.valueOf(annual)));
            FirebaseDatabase.getInstance().getReference().child("leavesquota").child(profileModel.getUid()).child("annual").setValue(leaveQoutaModel);
        }else{
            Toast.makeText(this,"Please enter the number only",Toast.LENGTH_LONG).show();
        }

    }
}