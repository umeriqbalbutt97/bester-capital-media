package com.smartherd.bestercapitalmedia.view;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.smartherd.bestercapitalmedia.R;
import com.smartherd.bestercapitalmedia.model.LeaveQoutaModel;
import com.smartherd.bestercapitalmedia.model.ProfileModel;
import com.smartherd.bestercapitalmedia.utils.Utilities;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.smartherd.bestercapitalmedia.utils.Utilities.CAMERA_REQUEST_CODE;
import static com.smartherd.bestercapitalmedia.utils.Utilities.GALLERY_REQUEST_CODE;

public class BasicProfileActivity extends BaseActivity {

    private ImageView photo;
    private Uri imageUri;
    private String currentPhotoPath;
    private FirebaseUser firebaseUser;
    private RadioGroup type;
    private int tflag=0;
    private EditText f_name,l_name;
    private DatePicker datePicker;
    private ProfileModel profileModel;
    private StorageReference mStorageRef;
    private StorageReference mImageRef;
    private StorageTask mStorageTask;
    private ProgressDialog progressDialog;
    private String url;
    private DatabaseReference databaseReference;
    private SharedPreferences sharedPreferences;
    private String uid;
    String token;

    @Override
    protected void onStart() {
        super.onStart();

        new Thread(new Runnable() {
            @Override
            public void run() {
                firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                while (!firebaseUser.isEmailVerified()){
                    firebaseUser.reload();
                }

            }
        }).start();

        SharedPreferences prefs = getSharedPreferences("TOKEN", MODE_PRIVATE);
        token = prefs.getString("tokenId", "noToken");

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_profile);
        generatedToken();

        init();

        selectTypeBuyerSeller();
    }

    private void init(){
        uid=FirebaseAuth.getInstance().getUid();
        photo = findViewById(R.id.photo);
        f_name=findViewById(R.id.f_name);
        sharedPreferences = getSharedPreferences("Check", Context.MODE_PRIVATE);
        l_name=findViewById(R.id.l_name);
        type=findViewById(R.id.groupradiotypeselection);
        datePicker=findViewById(R.id.datePicker);
        mStorageRef = FirebaseStorage.getInstance().getReference();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("Profiles");
        progressDialog = new ProgressDialog(this);
    }

    public void accountCreationFun(View view) {
        if(tflag!=0) {
            basicProfileInfoEntry();
        }else {
            Toast.makeText(this, "Please Verify and Select your role", Toast.LENGTH_SHORT).show();
        }
    }

    public void basicProfileInfoEntry(){
        String first,last,date;
        first=f_name.getText().toString();
        last=l_name.getText().toString();
        date= datePicker.getDayOfMonth() + "/" + (datePicker.getMonth() + 1) + "/" + datePicker.getYear();

        if(first.isEmpty()||last.isEmpty()||date.isEmpty()||tflag==0){
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show();
        }else{
            profileModel =new ProfileModel(first,last,date,null,tflag,null,FirebaseAuth.getInstance().getUid(),token);
            firebaseUploadingMechanism(profileModel);
        }

    }

    public void firebaseUploadingMechanism(ProfileModel profileModel) {
        if (profileModel != null) {
            uploadSingleImages();
        } else {
            Toast.makeText(getApplicationContext(), "Something goes Wrong", Toast.LENGTH_LONG).show();
        }
    }

    public void leaveQuotaAllotment(){
        LeaveQoutaModel leaveQoutaModel=new LeaveQoutaModel(8,38,0);
        FirebaseDatabase.getInstance().getReference().child("leavesquota").child(FirebaseAuth.getInstance().getUid()).child("sick").setValue(leaveQoutaModel);

        leaveQoutaModel.setRemaining_leaves(0);
        leaveQoutaModel.setTotal_leaves(12);
        FirebaseDatabase.getInstance().getReference().child("leavesquota").child(FirebaseAuth.getInstance().getUid()).child("casual").setValue(leaveQoutaModel);

        leaveQoutaModel.setRemaining_leaves(0);
        leaveQoutaModel.setTotal_leaves(12);
        FirebaseDatabase.getInstance().getReference().child("leavesquota").child(FirebaseAuth.getInstance().getUid()).child("annual").setValue(leaveQoutaModel);

        leaveQoutaModel.setRemaining_leaves(0);
        leaveQoutaModel.setTotal_leaves(6);
        FirebaseDatabase.getInstance().getReference().child("leavesquota").child(FirebaseAuth.getInstance().getUid()).child("short").setValue(leaveQoutaModel);
    }

    private void uploadSingleImages() {
        progressDialog.setMessage("Uploading...");
        progressDialog.show();
        progressDialog.setCanceledOnTouchOutside(false);
        String imageName = System.currentTimeMillis() + ".jpg";
        mImageRef = mStorageRef.child("ProfilePhotos/" + imageName);
        if (imageUri != null) {
            mStorageTask = mImageRef.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    if (mStorageTask.isComplete()) {
                        mImageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                url = uri.toString();
                                profileModel.setImg_url(url);
                                databaseReference.child(uid).setValue(profileModel);
                                leaveQuotaAllotment();
                                progressDialog.dismiss();
                                moveToUserInterface();
                            }
                        });
                    }
                }
            });
        }else {
            Toast.makeText(getApplicationContext(),"Please Select Image",Toast.LENGTH_LONG).show();
        }
    }

    public void selectImage(final View view) {
        final CharSequence charSequence[] = {"Camera", "Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(BasicProfileActivity.this);
        builder.setTitle("Take a Photo");
        builder.setItems(charSequence, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (charSequence[which].equals("Camera")) {
                    dispatchTakePictureIntent();
                } else if (charSequence[which].equals("Gallery")) {
                    openGallery(view);
                } else {
                    Toast.makeText(getApplicationContext(), "Something Wrong", Toast.LENGTH_LONG).show();
                }
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void openGallery(View view) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == GALLERY_REQUEST_CODE) {
            try {
                Uri selectedImage = data.getData();
                imageUri = selectedImage;
                InputStream imageStream = getContentResolver().openInputStream(selectedImage);
                photo.setImageBitmap(BitmapFactory.decodeStream(imageStream));
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }

        if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            File f = new File(currentPhotoPath);
            photo.setImageURI(Uri.fromFile(f));
            Uri contentUri = Uri.fromFile(f);
            imageUri = contentUri;
            mediaScanIntent.setData(contentUri);
            this.sendBroadcast(mediaScanIntent);
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
            }
        }
    }

    public void selectTypeBuyerSeller() {

        type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {


                //if you can not write the firebaseUser.reload f will always false

                boolean f = FirebaseAuth.getInstance().getCurrentUser().isEmailVerified();
                Log.d(Utilities.TAG, "onCheckedChanged: " + f + "->" + firebaseUser.getEmail());
                if (firebaseUser.isEmailVerified()) {
                    View radioButton = type.findViewById(checkedId);
                    int index = type.indexOfChild(radioButton);

                    // Add logic here

                    switch (index) {
                        case 0:
                            tflag=1;
                            break;
                        case 1:
                            tflag=2;
                            break;
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please Verify your Email", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void moveToUserInterface() {
        boolean isCheck = false;
        if (tflag == 1) {
            isCheck = true;
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putBoolean("CHK", isCheck);
            edit.putInt("GET", 1);
            edit.apply();
            Intent intent = new Intent(BasicProfileActivity.this, EmployeeHome.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else if (tflag == 2) {
            isCheck = true;
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putInt("GET", 2);
            edit.putBoolean("CHK", isCheck);
            edit.apply();
            Intent intent = new Intent(BasicProfileActivity.this, ManagerHome.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(getApplicationContext(), "Something Goes Wrong", Toast.LENGTH_LONG).show();
        }
    }

}