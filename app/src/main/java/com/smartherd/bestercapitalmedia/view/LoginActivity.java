package com.smartherd.bestercapitalmedia.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.smartherd.bestercapitalmedia.R;
import com.smartherd.bestercapitalmedia.utils.Utilities;

public class LoginActivity extends BaseActivity {

    private TextView email, password;
    private ProgressBar progressBar;
    private FirebaseAuth auth;
    private DatabaseReference rootRef, childRef;

    @Override
    protected void onStart() {
        super.onStart();
        generatedToken();
        FirebaseUser user = auth.getCurrentUser();
        if (user != null) {
            Intent intent = new Intent(LoginActivity.this, BasicProfileActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();

    }

    private void init(){
        auth=FirebaseAuth.getInstance();
        mPrefs = getSharedPreferences("Check", Context.MODE_PRIVATE);
        check = mPrefs.getBoolean("CHK", false);
        email=findViewById(R.id.input_email);
        password=findViewById(R.id.input_password);
        progressBar=findViewById(R.id.login_progress_bar);
        rootRef = FirebaseDatabase.getInstance().getReference();
        childRef = rootRef.child("Profiles");
    }

    public void toSignUp(View view) {
        Intent intent=new Intent(LoginActivity.this,SignUpActivity.class);
        startActivity(intent);
        finish();
    }

    public void forgotPasswordDialog(View view) {
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.custom_dialog_box_forgot_password, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.editTextDialogUserInput);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // get user input and set it to result
                                // edit text
                                String u_input=userInput.getText().toString();
                                if(u_input.isEmpty()){
                                    showToast(LoginActivity.this,"Please Enter Your Email");
                                }else {
                                    FirebaseAuth.getInstance().sendPasswordResetEmail(userInput.getText().toString())
                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {
                                                        Log.d(Utilities.TAG, "Email sent.");
                                                    }
                                                }
                                            });
                                }
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }

    public void logIn(View view) {
                String text_email = email.getText().toString();
                String text_password = password.getText().toString();


                if (TextUtils.isEmpty(text_email) || TextUtils.isEmpty(text_password)) {
                    showToast(LoginActivity.this, "All fields are required");
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    auth.signInWithEmailAndPassword(text_email, text_password)
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        progressBar.setVisibility(View.INVISIBLE);
                                        checkUserType();
                                    } else {
                                        // error handling
                                        if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                            progressBar.setVisibility(View.INVISIBLE);
                                            showToast(LoginActivity.this, "Password is incorrect");

                                        } else if (task.getException() instanceof FirebaseAuthInvalidUserException) {
                                            progressBar.setVisibility(View.INVISIBLE);
                                           showToast(LoginActivity.this, "Email doesn't exist");
                                        }
                                    }
                                }
                            });
                }
    }

    public void checkUserType() {
        childRef.child(auth.getUid()).child("role").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String type = String.valueOf(dataSnapshot.getValue());
                if(dataSnapshot.exists()) {

                    if (type.equals("1")) {
                        SharedPreferences.Editor editor = mPrefs.edit();
                        editor.putBoolean("CHK", true);
                        editor.putInt("GET", 1);
                        editor.apply();
                        FirebaseDatabase.getInstance().getReference().child("Profiles").child(auth.getUid()).child("token_id").setValue(token);
                        Intent intent = new Intent(LoginActivity.this, EmployeeHome.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    } else if (type.equals("2")) {
                        SharedPreferences.Editor editor = mPrefs.edit();
                        editor.putBoolean("CHK", true);
                        editor.putInt("GET", 2);
                        editor.apply();
                        FirebaseDatabase.getInstance().getReference().child("Profiles").child(auth.getUid()).child("token_id").setValue(token);
                        Intent intent = new Intent(LoginActivity.this, ManagerHome.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                    }
                }else{
                    showToast(LoginActivity.this,"Your Account has been deleted by admin");
                    auth.signOut();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                showToast(LoginActivity.this,databaseError.getMessage());
            }
        });
    }
}