package com.smartherd.bestercapitalmedia.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.smartherd.bestercapitalmedia.R;
import com.smartherd.bestercapitalmedia.adapter.AddEmployeeAdapter;
import com.smartherd.bestercapitalmedia.model.ProfileModel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AddEmployee extends AppCompatActivity {

    List<ProfileModel> profileModelList=new ArrayList<>();
    RecyclerView recyclerView;
    AddEmployeeAdapter addEmployeeAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_employee);

        recyclerView=findViewById(R.id.add_employee_recyclerview);
        getEmpDataFromFirebase();

    }

    public void bakeRecyclerView(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        addEmployeeAdapter=new AddEmployeeAdapter(this,profileModelList);
        recyclerView.setAdapter(addEmployeeAdapter);
    }

    public void getEmpDataFromFirebase(){
        FirebaseDatabase.getInstance().getReference().child("Profiles").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                profileModelList.clear();
                for(DataSnapshot child: snapshot.getChildren()){
                    ProfileModel profileModel=child.getValue(ProfileModel.class);
                    if(profileModel.getRole()==1) {
                        profileModelList.add(profileModel);
                    }
                }
                bakeRecyclerView();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }


}