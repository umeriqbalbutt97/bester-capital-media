package com.smartherd.bestercapitalmedia.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

public class LeaveModel implements Parcelable {
    String uid;
    String push_id;
    String reason;
    String final_status;
    Map<String, String> status=new HashMap<>();
    String type;
    String from;
    String to;
    String hours;

    public LeaveModel() {
    }

    protected LeaveModel(Parcel in) {
        uid = in.readString();
        push_id = in.readString();
        reason = in.readString();
        final_status = in.readString();
        type = in.readString();
        from = in.readString();
        to = in.readString();
        hours = in.readString();
    }

    public static final Creator<LeaveModel> CREATOR = new Creator<LeaveModel>() {
        @Override
        public LeaveModel createFromParcel(Parcel in) {
            return new LeaveModel(in);
        }

        @Override
        public LeaveModel[] newArray(int size) {
            return new LeaveModel[size];
        }
    };

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getPush_id() {
        return push_id;
    }

    public void setPush_id(String push_id) {
        this.push_id = push_id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getFinal_status() {
        return final_status;
    }

    public void setFinal_status(String final_status) {
        this.final_status = final_status;
    }

    public Map<String, String> getStatus() {
        return status;
    }

    public void setStatus(Map<String, String> status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public LeaveModel(String uid, String push_id, String reason, String final_status, Map<String, String> status, String type, String from, String to, String hours) {
        this.uid = uid;
        this.push_id = push_id;
        this.reason = reason;
        this.final_status = final_status;
        this.status = status;
        this.type = type;
        this.from = from;
        this.to = to;
        this.hours = hours;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(uid);
        dest.writeString(push_id);
        dest.writeString(reason);
        dest.writeString(final_status);
        dest.writeString(type);
        dest.writeString(from);
        dest.writeString(to);
        dest.writeString(hours);
    }
}
