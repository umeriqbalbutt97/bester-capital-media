package com.smartherd.bestercapitalmedia.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import java.util.Map;

public class ProfileModel implements Parcelable {
    String f_name,l_name,dob,img_url;
    int role;
    Map<String,Boolean> managers;
    String uid;
    String token_id;

    public ProfileModel(String f_name, String l_name, String dob, String img_url, int role, Map<String, Boolean> managers, String uid, String token_id) {
        this.f_name = f_name;
        this.l_name = l_name;
        this.dob = dob;
        this.img_url = img_url;
        this.role = role;
        this.managers = managers;
        this.uid = uid;
        this.token_id = token_id;
    }

    public String getToken_id() {
        return token_id;
    }

    public void setToken_id(String token_id) {
        this.token_id = token_id;
    }

    protected ProfileModel(Parcel in) {
        f_name = in.readString();
        l_name = in.readString();
        dob = in.readString();
        img_url = in.readString();
        role = in.readInt();
        uid = in.readString();
    }

    public static final Creator<ProfileModel> CREATOR = new Creator<ProfileModel>() {
        @Override
        public ProfileModel createFromParcel(Parcel in) {
            return new ProfileModel(in);
        }

        @Override
        public ProfileModel[] newArray(int size) {
            return new ProfileModel[size];
        }
    };

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Map<String, Boolean> getManagers() {
        return managers;
    }

    public void setManagers(Map<String, Boolean> managers) {
        this.managers = managers;
    }


    public ProfileModel() {
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getF_name() {
        return f_name;
    }

    public void setF_name(String f_name) {
        this.f_name = f_name;
    }

    public String getL_name() {
        return l_name;
    }

    public void setL_name(String l_name) {
        this.l_name = l_name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(f_name);
        dest.writeString(l_name);
        dest.writeString(dob);
        dest.writeString(img_url);
        dest.writeInt(role);
        dest.writeString(uid);
    }
}
