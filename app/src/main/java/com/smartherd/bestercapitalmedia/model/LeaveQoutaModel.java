package com.smartherd.bestercapitalmedia.model;

public class LeaveQoutaModel {
    int total_leaves;
    int max;

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public LeaveQoutaModel(int total_leaves, int max, int remaining_leaves) {
        this.total_leaves = total_leaves;
        this.max = max;
        this.remaining_leaves = remaining_leaves;
    }

    public LeaveQoutaModel() {
    }

    public int getTotal_leaves() {
        return total_leaves;
    }

    public void setTotal_leaves(int total_leaves) {
        this.total_leaves = total_leaves;
    }

    public int getRemaining_leaves() {
        return remaining_leaves;
    }

    public void setRemaining_leaves(int remaining_leaves) {
        this.remaining_leaves = remaining_leaves;
    }

    public LeaveQoutaModel(int total_leaves, int remaining_leaves) {
        this.total_leaves = total_leaves;
        this.remaining_leaves = remaining_leaves;
    }

    int remaining_leaves;
}
