package com.smartherd.bestercapitalmedia.notifications;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.smartherd.bestercapitalmedia.R;
import com.smartherd.bestercapitalmedia.view.SplashActivity;

public class MyFirebaseMessagingService extends FirebaseMessagingService {



    public MyFirebaseMessagingService() {
    }


    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
//            String title=remoteMessage.getNotification().getTitle();
//            String body=remoteMessage.getNotification().getBody();


            if (remoteMessage.getData().size() > 0) {

                if (/* Check if data needs to be processed by long running job */ true) {
                    Log.d("firebaseServices", "onMessageReceived: " + "in firebase func");
                    // For long-running tasks (10 seconds or more) use WorkManager.
                    displayNotification(getApplicationContext(),  remoteMessage.getData().get("title"), remoteMessage.getData().get("body"));
                } else {
                    // Handle message within 10 seconds

                }

            }

//

    }

    public  void displayNotification(Context context, String title, String body){

        Intent intent = new Intent(context, SplashActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),5146,intent,PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder=new NotificationCompat.Builder(context, "2222")
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(body))
                .setContentText(body)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        NotificationManagerCompat notificationManagerCompat=NotificationManagerCompat.from(context);
        notificationManagerCompat.notify(1,mBuilder.build());
    }
}
